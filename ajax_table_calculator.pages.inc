<?php

function _wrapper_debug($w) {
  $values = array();
  foreach ($w->getPropertyInfo() as $key => $val) {
    $values[$key] = $w->$key->value();
  }
  return $values;
}

/** * Page callback: Constructs a form for the ‘My Products’ page. 
* 
* Path: list-my-products 
* 
* @see ajax_table_calculator_menu() 
*/ 
function ajax_table_calculator_my_products_form($form, &$form_state) { 
//global $user; 

// Initialise the form array. 
$form = array( 
	'#tree' => TRUE, 
); 
// Define the table header. This is used for the EntityFieldQuery and for 
// the theme_table() header. 
$header = array( 

'Checkbox', 

array( 
'data' => 'Title', 
'type' => 'property', 
'specifier' => 'title', 
), 
array( 
'data' => 'Price', 
'type' => 'field', 
'specifier' => array( 
'field' => 'commerce_price', 
'column' => 'amount', 
), 
), 
//'Operations', 
);

// Get all the product variants (i.e. commerce_product entities) for the logged-in user. 
$query = new EntityFieldQuery(); 
$query->entityCondition('entity_type', 'commerce_product') 
		->propertyCondition('type', 'web_services')
		->tableSort($header) ->pager(5);
$result = $query->execute(); 
// $header needs to be saved AFTER the EntityFieldQuery. 
$form['#header'] = $header; 
$products = array(); 
if (isset($result['commerce_product'])) { 
$products = 
commerce_product_load_multiple(array_keys($result['commerce_product'])); 
}

$product_statuses = commerce_product_status_options_list(); 

$form['products'] = array(); 
$total = '0';
dsm($products);
foreach ($products as $product) { 
//dsm($product);
$product_wrapper = entity_metadata_wrapper('commerce_product', $product); 
//dsm($product_wrapper);
$product_id = $product_wrapper->product_id->value(); 
$price = $product_wrapper->commerce_price->value(); 
/*
$form['products'][$product_id]['title'] = array( 
											'#type' => 'item', 
											'#markup' => l($product_wrapper->title->value(), 'admin/commerce/products/' . $product_id), 
																	); 
*/
//dpm($form['products'][$product_id]);
$form['products'][$product_id]['title'] = array( 
											'#type' => 'item', 
											'#markup' => l($product_wrapper->title->value(), 'admin/commerce/products/' . $product_id), 
																	); 
$form['products'][$product_id]['price'] = array( 
											'#type' => 'item', 
											'#markup' => $price['amount'],
											//commerce_currency_format($price['amount'], $price['currency_code']), 
											);

/*******************************Checkboxes****************************************/

$form['products'][$product_id]['checkbox'] = array(
      '#type' => 'checkbox',
      '#ajax' => array(
			'callback' => 'ajax_table_calculator_form_refresh',
			'method' => 'replace',
			'progress' => 'none',
			'wrapper' => 'result',
      ),
    );
//$form_state['var'] = array('variable' => 'My_costome_variable');
/*********************************************************************************/
 }
 $form['result'] = array(
    '#markup' => '<div id="result">Result: ' . commerce_currency_format($total,'EUR'). '</div>',
  );
 return $form; 
 }


function ajax_table_calculator_form_refresh($form, $form_state) {
$total = '500';
//$values= $form_state;
//dsm($values);
$test = array();
dsm($form_state['values']['products']);
foreach ($form_state['values']['products'] as $key=>$prod) {

//$prod_wr = entity_metadata_wrapper('commerce_product', $prod); 	
//	$pr = $prod_wr->commerce_price->value();
	$test[$key] = $prod;
	}
	//print_r($test);
	
dsm($test);

$form['result'] = array(
    '#markup' => '<div id="result">Result: ' . commerce_currency_format($total,'EUR'). '</div>',
  );
  return $form['result'];
} 
 
 