<?php
/**
 * Returns HTML for the 'My Products' page.
 *
 * @param array $variables
 *   An associative array containing:
 *   - form: The Form API render array.
 */
function theme_ajax_table_calculator_my_products_form($variables) {
  $form = $variables['form'];
  $header = $form['#header'];
  $rows = array();

/*****************Default******************************/

  foreach (element_children($form['products']) as $key) {
    $row_data = array(
		drupal_render($form['products'][$key]['checkbox']),
      drupal_render($form['products'][$key]['title']),
      drupal_render($form['products'][$key]['price']),
    );
    $rows[] = array('data' => $row_data);
  }

/******************************************************/

	$output = theme('table', array('header' => $header, 'rows' => $rows));
	$output .= theme('pager');
	$output .= drupal_render_children($form);

  return $output;
}